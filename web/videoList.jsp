<%@ page import="java.util.List" %>
<%@ page import="com.jeemvc.Video" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Video List</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/simple-sidebar.css" rel="stylesheet">

</head>

<body>

<div id="wrapper" class="toggled">

    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <li class="sidebar-brand">
                <a href="index.jsp">Welcome Page</a>
            </li>
            <li>
                <a href="videoAdd.jsp">Add File</a>
            </li>
            <li>
                <a href="video">Manage File</a>
            </li>
        </ul>
    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">
        <div class="container-fluid">
            <h1>File Manager</h1>
            <br>


            <table>

                <%
                    List<Video> videos = (List<Video>) request.getAttribute("videos");
                    for (Video video : videos) {

                %>

                <tr>
                    <td>
                        <%
                            if (video.getTitle().contains(".mp4")) {
                        %>
                        <video width="200" height="200" controls>
                            <source src="<%=video.getServerPath()%>" type="video/mp4">
                        </video>
                        <%
                            }
                            if (video.getTitle().contains(".jpg") || video.getTitle().contains(".png")) {
                        %>

                        <img src="<%=video.getServerPath()%>" width="200" height="200">

                        <%
                            }
                            if (video.getTitle().contains(".zip")) {
                        %>

                        <p>This is .zip file: <%=video.getTitle()%>
                        </p>

                        <%
                            }
                        %>
                    </td>
                    <td>
                        <form method="post" action="download">
                            <input hidden="true" name="path" type="text" value="<%=video.getResourcePath()%>">
                            <input hidden="true" name="videoname" type="text" value="<%=video.getTitle()%>">
                            <input type="submit" value="Download"/>
                        </form>
                    </td>
                    <td>
                        <form method="post" action="remove">
                            <input hidden="true" name="path" type="text" value="<%=video.getResourcePath()%>">
                            <input hidden="true" name="videoname" type="text" value="<%=video.getTitle()%>">
                            <input type="submit" value="Delete"/>
                        </form>
                    </td>
                </tr>

                <%
                    }
                %>

            </table>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>

</body>

</html>
