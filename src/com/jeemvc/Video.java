package com.jeemvc;

import java.io.InputStream;

public class Video {

	private String title;
	private InputStream content;
	private String resourcePath;
	private String serverPath;

	public Video(String title, InputStream content, String resourcePath, String serverPath) {
		this.title = title;
		this.content = content;
		this.resourcePath = resourcePath;
		this.serverPath = serverPath;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public InputStream getContent() {
		return content;
	}

	public void setContent(InputStream content) {
		this.content = content;
	}


	public String getServerPath() {
		return serverPath;
	}

	public void setServerPath(String serverPath) {
		this.serverPath = serverPath;
	}
}
