package com.jeemvc.dao;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

import com.jeemvc.Video;

@Stateless
public class VideoDao {

	List<Video> videos = new ArrayList<>();

	@PostConstruct
	private void init() {

	}

	public List<Video> getAll() {
		return videos;
	}

	public void add(String fileName, InputStream fileContent, String pathToResource, String serverPath) {
		videos.add(new Video(fileName, fileContent, pathToResource + "/" + fileName, String.format(serverPath, fileName)));
	}

	public void remove(String videoName) {
		int index = 0;

		for(Video video : videos) {
			if (video.getTitle().equals(videoName)) {
				index = videos.indexOf(video);
			}
		}
		videos.remove(index);
	}
}