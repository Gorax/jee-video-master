package com.jeemvc.servlets;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeemvc.Video;
import com.jeemvc.dao.VideoDao;

@WebServlet("/video")
public class GetVideoServlet extends HttpServlet {

	@EJB
	private VideoDao videoDao;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Video> allVideos = videoDao.getAll();
		req.setAttribute("videos", allVideos);
		req.getRequestDispatcher("videoList.jsp").forward(req, resp);
	}

}
