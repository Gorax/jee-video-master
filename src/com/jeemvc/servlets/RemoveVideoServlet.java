package com.jeemvc.servlets;

import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeemvc.dao.VideoDao;

@WebServlet("/remove")
@MultipartConfig
public class RemoveVideoServlet extends HttpServlet {

	@EJB
	private VideoDao videoDao;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String videoPath = request.getParameter("path");
		String videoName = request.getParameter("videoname");

		try {
			Files.delete(Paths.get(videoPath));
		} catch (NoSuchFileException x) {
			System.err.format("%s: no such" + " file or directory%n", videoName);
		} catch (DirectoryNotEmptyException x) {
			System.err.format("%s not empty%n", videoName);
		} catch (IOException x) {
			System.err.println(x);
		}

		getServletContext().getRequestDispatcher("/done.jsp").forward(
				request, response);

		videoDao.remove(videoName);
	}
}
