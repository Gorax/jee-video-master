package com.jeemvc.servlets;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.jeemvc.dao.VideoDao;

@WebServlet("/add")
@MultipartConfig
public class AddVideoServlet extends HttpServlet {

	public String glassfishResource = new File("").getAbsolutePath();
	//public String glassfishResource = "/glassfish5/glassfish/domains/domain1/docroot";
	public String serverPath = "http://localhost:8080/%s";


	@EJB
	private VideoDao videoDao;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pathToResource = glassfishResource.replaceAll("config", "docroot");
		Part filePart = request.getPart("file");
		String fileName = UUID.randomUUID().toString() + "-" + Paths.get(filePart.getSubmittedFileName()).getFileName().toString();
		InputStream fileContent = filePart.getInputStream();

		File uploads = new File(pathToResource);
		File file = new File(uploads, fileName);

		try (InputStream input = fileContent) {
			Files.copy(input, file.toPath());
		}

		videoDao.add(fileName, fileContent, pathToResource, serverPath);

		getServletContext().getRequestDispatcher("/done.jsp").forward(
				request, response);
	}
}
